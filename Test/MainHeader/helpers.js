function getPageInfo (pages, index) {
    const group = pages[index].group    
    let gstart = index
    while (gstart - 1 > -1 && pages[gstart - 1].group === group) {
        gstart--
    }
    let gend = index
    while ((gend + 1 < pages.length) && pages[gend + 1].group === group) {
        gend++
    }           
    return (index - gstart + 1) + ' of ' + (gend - gstart + 1)
}

function getPageNumber (pageIndex) {
    if (pageIndex == null) {
        return ''
    }
    const pageNumber = pageIndex + 1
    return "p" + pageNumber
}

Handlebars.registerHelper("renderHeader", function(root, options){ 
    var ret = {}
    var headerconfig = root.headerconfig
    var headerdata = root.headerdata.customFields

    headerconfig.layout.sections.forEach(headeritem =>{
        switch(headeritem.title){
            case "General": { 
                var general = {}
                headeritem.fields.forEach((fielditem, index) =>{                                
                    general["title"+(index+1)] = fielditem.title
                    general["data"+(index+1)] = headerdata[fielditem.key]
                })
                ret = {
                    ...ret,
                    general
                }
                break
            }
            case "Personnel": {  
                var personal = {}
                var date = new Date()
                var d = ((date.getMonth() > 8) ? (date.getMonth() + 1) : ('0' + (date.getMonth() + 1))) + '-' + ((date.getDate() > 9) ? date.getDate() : ('0' + date.getDate())) + '-' + date.getFullYear()

                personal.technician = headerdata[headeritem.fields[0].key] + " " + headerdata[headeritem.fields[1].key] + " " + d.toString()
                personal.customername = headerdata[headeritem.fields[2].key]
                personal.customersign = renderSvg(headerdata[headeritem.fields[3].key] ? headerdata[headeritem.fields[3].key].signatureSvg : "")
                personal.reviewername = headerdata[headeritem.fields[4].key]
                personal.reviewersign = renderSvg(headerdata[headeritem.fields[5].key] ? headerdata[headeritem.fields[5].key].signatureSvg : "")
                ret = {
                    ...ret,
                    personal
                }
                break
            }
        }
    })
    return options.fn(ret)
})

function renderSvg(svgString) {
    return new Handlebars.SafeString(svgString)
}

