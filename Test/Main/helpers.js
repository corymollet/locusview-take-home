
// organize and set the data
Handlebars.registerHelper("eachHeader", function(config, data, options){
    var ret = {};
    var sections = config.layout.sections; //For Acuren at this time - (1-st element)
    var headerdata = data.customFields //For Acuren at this time - (1-st element)
    sections.forEach(item=>{
        switch(item.title){
            case 'Test Specifications': {
                ret = {
                    ...ret,
                    "testspec": item.fields.map(ele=>{
                        return {"data": headerdata[ele.key]};
                    })
                }
                break;
            }
            case 'Light Meters': {
                ret = {
                    ...ret,
                    "lightmeter": item.fields.map(ele=>{
                        var data = headerdata[ele.key]
                        switch(ele.title){
                            case 'Medium':{
                                data = ele.options.map(e=>{
                                    var selected = "";
                                    var keys = headerdata[ele.key];
                                    for(var s = 0; s < keys.length; s++){
                                        if(e.title==keys[s]){
                                            selected = "checked"
                                        }
                                    }
                                    return "<span><input type='checkbox' "+selected+"/>" + e.title+"</span>";
                                })
                                break;
                            }
                        }
                        return {"data": data}
                    })
                }
                break;
            }
            case 'Particles': {
                ret = {
                    ...ret,
                    "particle": item.fields.map(ele=>{
                        return {"data": headerdata[ele.key]};
                    })
                }
                break;
            }
            case 'Vehicle': {
                ret = {
                    ...ret,
                    "vehicle": item.fields.map(ele=>{
                        return {"data": headerdata[ele.key]};
                    })
                }
                break;
            }
            case 'Technique': {
                ret = {
                    ...ret,
                    "technique": item.fields.map(ele=>{
                        var data = headerdata[ele.key]
                        if(ele.title == "Current" || 
                           ele.title == "Technique Data")
                        {
                            data = ele.options.map(e=>{
                                var selected = "";
                                if(e.title==headerdata[ele.key]){
                                    selected = "checked"
                                }
                                return "<span><input type='checkbox' "+selected+"/>" + e.title + "</span>";
                            })
                        }
                        return {"data": data}
                    })
                }
                break;
            }
            case 'Test Results': {
                ret = {
                    ...ret,
                    "testres": item.fields.map(ele=>{
                        return {"data": headerdata[ele.key]};
                    })
                }
                break;
            }
        }
    });
    return options.fn(ret);
});

function is_NON(val){
    return val?val:"N/A"
}

// The amount of rows for a "full" page
const ROWS_PER_PAGE = 39;

function sortFormConfigFields(sections) {
    let sortedFields = [];
    if (sections.length === 1) {
        sortedFields = sections[0].fields.sort((a, b) => {
            return a.order - b.order;
        });
    }
    return sortedFields;
}

function createPageRows(rows, index = 0, perPage = ROWS_PER_PAGE) {
    return rows.slice(index, index + perPage);
}

function prepareData(formdata, formdataconfig) {
    /*
    Prepare data by matching the ordered custom fields in formdataconfig
    to the actual rows of data in formdata. Will return an array of arrays
    of the values of the custom fields in the customFields object of formdata.

    @param formdata: parsed result of data/homework/templates/Test/data/formdata/dataJson.json
    @param formdataconfig: parsed result of data/homework/templates/Test/data/formconfig/dataJson.json
    */
    let data = [];
    const sortedSections = sortFormConfigFields(formdataconfig.layout.sections);

    const customFieldOrder = sortedSections.map(elem => elem.key);
    formdata.forEach(item => {
        const fieldData = item.customFields;

        let row = [];
        customFieldOrder.forEach(field => {
            row.push(fieldData[field])
        })


        data.push(row);

    })


    return data;
}


function setData(formdata, formdataconfig){
    let data = [];
    // Start
    const preparedData = prepareData(formdata, formdataconfig);

    // first page only has room for 23
    let startRows = 23;
    data.push(createPageRows(preparedData, 0, startRows));

    while (startRows <= preparedData.length) {
        data.push(createPageRows(preparedData, startRows, ROWS_PER_PAGE))
        startRows = startRows + ROWS_PER_PAGE;
    }
    // End

  

    this.newformdata = data
}