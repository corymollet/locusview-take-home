const path = require('path')  
const promisify= require('util').promisify
const readFileAsync = promisify(require('fs').readFile)

// pulls in the data for the report
// Runs before the template
async function beforeRender(req, res, done)  { 
    const headerconfigpath = path.join(__appDirectory,  'data/templates/Test/data/headerconfig/dataJson.json')
    const headerconfigdata = (await readFileAsync(headerconfigpath)).toString()
    req.data.headerconfig = JSON.parse(headerconfigdata)

    const headerdatapath = path.join(__appDirectory,  'data/templates/Test/data/headerdata/dataJson.json')
    const headerdata = (await readFileAsync(headerdatapath)).toString()
    req.data.headerdata = JSON.parse(headerdata)

    const formdataconfigpath = path.join(__appDirectory,  'data/templates/Test/data/formconfig/dataJson.json')
    const formdataconfig = (await readFileAsync(formdataconfigpath)).toString()
    req.data.formdataconfig = JSON.parse(formdataconfig)

    const formdatapath = path.join(__appDirectory,  'data/templates/Test/data/formdata/dataJson.json')
    const formdata = (await readFileAsync(formdatapath)).toString()
    req.data.formdata = JSON.parse(formdata)
    
    done()
}